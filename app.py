import quart
import types
import asyncio
import functools
import json
import nats
import nats_json_rpc
import copy

app = quart.app.Quart(__name__)

rpc_funcs = []
def rpc_register(func):
    rpc_funcs.append(func)
    return func

@app.before_serving
async def nats_open():
    app.nats = types.SimpleNamespace()
    app.nats.connection = await nats.connect('nats://nats')
    app.nats.rpc = nats_json_rpc.NatsJsonRPC(app.nats.connection, 'structure.rpc.')

    for func in rpc_funcs:
        app.nats.rpc.rpc_register(func)

    await app.nats.rpc.rpc_init()

def filter_applies(parts, subject, expected):
    if parts == []:
        return str(subject) == expected
    current_part = parts.pop(0)
    try:
        subject = subject[current_part]
    except KeyError:
        return False
    
    if isinstance(subject, list):
        for element in subject:
            if filter_applies(parts, element, expected):
                return True
        return False
    
    return filter_applies(parts, subject, expected)

def inject_node(node_id_param='node_id', is_list=False, include_attributes=False, parse_includes=False, request_keywork='request', allow_multiple=False, calculate_attribute_display=True):
    def decorator_inject_node(func):
        @functools.wraps(func)
        async def replacement(*args, **kwargs):
            request = getattr(quart, request_keywork)
            try:
                request.args
            except:
                request = None
            multiple = allow_multiple

            _is_list = is_list
            _include_attributes = include_attributes

            if _is_list:
                node_id = request.args['id']
            else:
                node_id = kwargs[node_id_param]
                multiple &= (node_id == 'multiple')
                if multiple:
                    node_id = request.args['id']
                    _is_list = True

            if parse_includes or multiple:
                includes = request.args.getlist('include')
                include_children = 'children' in includes
                _include_attributes |= 'attributes' in includes

            filters = []
            if request is not None:
                filters = list(filter(lambda arg: arg.startswith('filter'), request.args.keys()))

            node = None

            kwargs.pop(node_id_param, None)

            if _is_list:
                nodes = await app.nats.rpc.proxy.nodes_get(
                    _id=node_id,
                    recursive=include_children,
                    include_attributes=_include_attributes,
                    calculate_attribute_display=calculate_attribute_display
                )
                def filter_matches(node):
                    attributes = node.get('attributes')
                    if attributes is not None:
                        node['attributes'] = {
                            attribute['id']: attribute.get('value')
                            for attribute in attributes
                        }
                    for filter_key in filters:
                        if not filter_applies(filter_key.split('.')[1:], node, request.args[filter_key]):
                            node['attributes'] = attributes
                            return False
                    node['attributes'] = attributes
                    return True

                node = list(filter(filter_matches, nodes))
            else:
                node = await app.nats.rpc.proxy.node_get(
                    _id=node_id,
                    include_attributes=_include_attributes,
                    calculate_attribute_display=calculate_attribute_display
                )

            if node == None:
                return f'Node "{node_id}" not found', 404

            if multiple:
                responses = []
                for node in node:
                    try:
                        response = await func(node, *args, **kwargs)
                        code = 200
                        if isinstance(response, tuple):
                            response = response[0]
                            code = response[1]
                        responses.append({
                            'node': node['_id'],
                            'response': response,
                            'code': code
                        })
                    except Exception as e:
                        responses.append({
                            'node': node['_id'],
                            'exception': str(e),
                        })
                return {
                    'responses': responses
                }

            return await func(node, *args, **kwargs)

        return replacement
    return decorator_inject_node

def timeout(timeout=5):
    def decorator_timeout(func):
        @functools.wraps(func)
        async def replacement(*args, **kwargs):
            try:
                async with asyncio.timeout(timeout):
                    return await func(*args, **kwargs)
            except asyncio.TimeoutError:
                return 'Backend timed out', 504

        return replacement

    return decorator_timeout

@app.after_serving
async def nats_close():
    await app.nats.connection.close()
    
@app.get('/api/nodes/<string:node_id>')
@inject_node(parse_includes=True)
async def node_get(node: dict):
    return node

@app.get('/api/nodes')
@inject_node(is_list=True, parse_includes=True)
async def node_get_root(nodes):
    return nodes

@app.get('/api/nodes/<string:node_id>/accounts')
@inject_node()
@timeout()
async def node_accounts_get(node: dict):
    result = await app.nats.rpc.proxy.users_get_by_node(node=node)
    def copy_by_fields(object, fields):
        result = {}
        for field in fields:
            try:
                result[field] = object[field]
            except KeyError:
                pass
        return result
        
    def mask_password(account):
        return copy_by_fields(account, ['_id', 'type', 'label', 'username', 'permissions', 'rootNode'])

    return list(map(mask_password, result))

@app.post('/api/nodes/<string:node_id>/accounts')
@inject_node()
@timeout()
async def node_accounts_create(node: dict):
    data = await quart.request.json
    type = data['type']
    permissions = data['permissions']
    label = data['label']

    user = await app.nats.rpc.proxy.auth_user_get(
        args=dict(quart.request.args), 
        headers=dict(quart.request.headers), 
        cookies=dict(quart.request.cookies)
    )

    # Check if user himself has sufficient permissions
    # one could argue that this belongs in the auth module, but that has no access to the request body...
    for key, value_requested in permissions.items():
        if value_requested and not user['permissions'].get(key):
            return 'User does not have permission {key} himself', 403

    result = await app.nats.rpc.proxy.users_create_invitation(node=node, type=type, label=label, permissions=permissions)
    return {
        'status': 'ok',
        'data': result
    }, 201

@app.delete('/api/nodes/<string:node_id>/accounts/<string:account_id>')
@inject_node()
@timeout()
async def node_accounts_delete(node: dict, account_id: str):
    result = await app.nats.rpc.proxy.users_delete(account_id=account_id)
    return {
        'status': 'ok'
    }

@app.get('/api/nodes/<string:node_id>/accounts/<string:account_id>/token')
@timeout()
async def account_key(node_id: str, account_id: str):
    return await app.nats.rpc.proxy.access_token_generate(account_id=account_id)

@app.get('/api/nodes/<string:node_id>/attributes')
@inject_node(include_attributes=True)
@timeout()
async def node_attributes_get(node: dict):
    attributes = node['attributes']
    result = {}
    for attribute in attributes:
        try:
            result[attribute['id']] = attribute['value']
        except KeyError:
            # nothing to do, no value present
            pass
    return result

class JsonataException(Exception):
    pass
class AttributeTypeException(Exception):
    pass
class AttributeFilterException(JsonataException):
    message = 'filter query not met'
    pass
class AttributeFilterFailedException(AttributeFilterException):
    pass
class AttributeTransformationException(JsonataException):
    pass
class AttributeValidationException(JsonataException):
    pass

def attribute_type_convert(attribute, value):
    value_type = attribute.get('value_type')
    if value_type == 'boolean':
        if isinstance(value, bool):
            # already a boolean value
            return value
        if value.lower() in ['true', '1']:
            return True
        if value.lower() in ['false', '0']:
            return False
        raise AttributeTypeException(f'value "{value}" cannot be converted to boolean')
    
    if value_type == 'integer':
        try:
            value = int(value)
        except ValueError:
            raise AttributeTypeException(f'value "{value}" cannot be converted to an integer')
        
        try:
            min = attribute['range']['min']
            max = attribute['range']['max']
            if (min < max) and not (min <= value <= max):
                raise AttributeTypeException(f'value {value} bust be >= {min} and <= {max}')
        except KeyError:
            pass

        return value
    
    if value_type == 'float':
        try:
            return float(value)
        except ValueError:
            raise AttributeTypeException(f'value "{value}" cannot be converted to a float')
        
    return value

async def node_attribute_validate(node, attribute, value):
    if attribute.get('filter') or attribute.get('validation') or attribute.get('transformation'):
        result = await app.nats.rpc.proxy.jsonata_verify(attribute=attribute, value=value)
    else:
        result = copy.copy(attribute) | {
            'value': value,
            'key': attribute['id'],
            'valid': True
        }

    result['value'] = attribute_type_convert(attribute, result['value'])
    
    return result

async def attribute_update(node, attribute, attribute_value):
    try:
        attribute_payload = await node_attribute_validate(node, attribute, attribute_value)
        return {
            'state': 'ok'
        }, attribute_payload
    except Exception as e:
        return {
            'state': 'error',
            'error': str(e)
        }, None

@app.put('/api/nodes/<string:node_id>/attributes/<string:attribute_key>')
@app.post('/api/nodes/<string:node_id>/attributes/<string:attribute_key>')
@rpc_register
@inject_node(include_attributes=True, allow_multiple=True, calculate_attribute_display=False)
@timeout()
async def node_attribute_set(node: dict, attribute_key: str, attribute_value=None):
    attributes = node.get('attributes', [])

    if attribute_value is None:
        attribute_value = await quart.request.get_data()
        attribute_value = attribute_value.decode()

    if attribute_key == 'all': 
        updates = []
        states = {}
        
        for attribute in attributes:
            state, update = await attribute_update(node, attribute, attribute_value)
            states[attribute['id']] = state
            if update:
                updates.append(update)
        
        await app.nats.rpc.proxy.node_attributes_update(
            node=node,
            attributes=updates
        )
        return {
            'states': states
        }
        
    attribute = None
    for attribute_ in attributes:
        if attribute_['id'] == attribute_key:
            attribute = attribute_
            break

    if attribute is None:
        return f'attribute {attribute_key} not found in node {node["_id"]}', 404

    try:
        payload = await node_attribute_validate(node, attribute, attribute_value)
        return await app.nats.rpc.proxy.node_attributes_update(
            node=node,
            attributes=[payload]
        )
    except Exception as e:
        # this should only catch JsonataExceptions
        # but RPC doesn't differentiate between exceptions ATM
        if quart.request.args.get('reflect_error', 'true').lower() in ["0", "false"]:
            return str(e), 200
        return str(e), 400


@app.patch('/api/nodes/<string:node_id>/attributes')
@app.post('/api/nodes/<string:node_id>/attributes')
@rpc_register
@inject_node(include_attributes=True, allow_multiple=True, calculate_attribute_display=False)
@timeout()
async def node_attributes_set(node: dict, attributes=None):
    if attributes is not None:
        request_payload = attributes
    elif quart.request.content_type == 'application/json':
        request_payload = await quart.request.json
    else:
        request_payload = await quart.request.form
    attributes = node['attributes']

    updates = []
    states = {}
    
    attributes_map = { attribute['id']: attribute for attribute in attributes}

    for id in request_payload.keys():
        try:
            attribute_value = request_payload[id]
        except KeyError:
            states[id] = {
                'state': 'error',
                'error': f'attribute "{id}" not found'
            }
            continue

        states[id], update = await attribute_update(node, attributes_map[id], attribute_value)
        if update:
            updates.append(update)
    
    await app.nats.rpc.proxy.node_attributes_update(
        node=node,
        attributes=updates
    )
    return {
        'states': states
    }

@app.get('/api/nodes/<string:node_id>/attributes/<string:attribute_key>')
@inject_node(include_attributes=True)
@timeout()
async def node_attribute_get(node: dict, attribute_key: str):
    for attribute in node['attributes']:
        if attribute['id'] == attribute_key:
            value = attribute['value']
            # convert "True" and "False" to "true"...
            if isinstance(value, bool):
                return str(value).lower()
            return str(value)
    
    return f'Attribute {node["_id"]}:{attribute_key} not found', 404

@app.websocket('/api/nodes/<string:node_id>/updates')
@inject_node(request_keywork='websocket')
async def node_updates(node: dict):
    topic = f'{node["_id"]}'
    includes = quart.websocket.args.getlist('include')
    if node["parentPath"] != '':
        topic = f'{node["parentPath"]}/{node["_id"]}'
        topic = topic[1:].replace('/', '.')

        if 'children' in includes:
            topic += '.>'

    async def message_cb(msg):
        decoded = json.loads(msg.data)

        if decoded['subject'] == 'node.attributes' and not include_duplicates:
            attributes = decoded['data']['node']['attributes']
            decoded['data']['node']['attributes'] = list(filter(lambda attribute: attribute['changed'], attributes))
            if(len(decoded['data']['node']['attributes']) == 0):
                # not real attribute updates, cancel update
                return

        await quart.websocket.send(json.dumps(decoded))
        
    await app.nats.connection.subscribe(
        f'structure.nodes.updates.{topic}',
        cb=message_cb
    )

    include_duplicates = 'duplicates' in includes

    await quart.websocket.accept()
    
@app.delete('/api/nodes/<string:node_id>')
@inject_node()
@timeout()
async def node_delete(node: dict):
    await app.nats.rpc.proxy.node_delete(
        _id=node['_id'],
        node=node,
    )
    return {
        'result': 'ok'
    }

async def node_update_ensure_validation(node_old: dict, node_new: dict):
    for old_attribute in node_old.get('attributes', []):
        for new_attribute in node_new.get('attributes', []):
            if old_attribute['id'] != new_attribute['id']:
                continue
            if old_attribute.get('validation', {}) == new_attribute.get('validation', {}):
                continue
            try:
                value = old_attribute['value']
            except KeyError:
                # no value present
                return
            payload = await node_attribute_validate(node_new, new_attribute, value)
            return await app.nats.rpc.proxy.node_attributes_update(
                node=node_old,
                attributes=[payload]
            )
    
@app.put('/api/nodes/<string:node_id>')
@inject_node(include_attributes=True)
@timeout()
async def node_set(node: dict):
    node_new = await quart.request.json

    await node_update_ensure_validation(node, node_new)

    result = await app.nats.rpc.proxy.node_set(
        _id=node['_id'],
        node=node_new
    )
    return {
        'status': 'ok',
        'data': result
    }
    
@app.patch('/api/nodes/<string:node_id>')
@inject_node(include_attributes=True)
@timeout()
async def node_patch(node: dict):
    node_new = await quart.request.json

    await node_update_ensure_validation(node, node_new)
    
    result = await app.nats.rpc.proxy.node_update(
        _id=node['_id'],
        node=node_new
    )
    return {
        'status': 'ok',
        'data': result
    }

@app.post('/api/nodes/<string:node_id>/children')
@inject_node()
@timeout()
async def node_create(node: dict):
    child = await quart.request.json
    child['parentPath'] = f'{node["parentPath"]}/{node["_id"]}'
    try:
        result = await app.nats.rpc.proxy.node_create(
            node=child
        )
        return {
            'status': 'ok',
            'data': result
        }, 201
    except Exception as e:
        return {
            'error': str(e)
        }, 500
